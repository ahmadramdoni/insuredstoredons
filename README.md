InsuredStoreDons adalah Store yang dapat digunakan untuk mempermudah Pembelian Asuransi Anda

Bagaimana cara membuat akun ?
1. Register untuk buat akun
2. Login dengan akun anda
3. Enjoyed with Our Store

Bagaimana cara membeli Polis ?
1. Jangan Lupa Login
2. Pilih Asuransi apa yang akan dibeli
3. Kemudian klik "Beli"
4. Asuransi pilihan anda sudah masuk menu "Cart"
5. Silahkan klik "Checkout" untuk melakukan validasi pembelian
6. Kemudian anda ke Bank untuk melakukan pembayaran Polis
7. Satu kali pembayaran hanya untuk satu Polis saja
8. Kemudian anda Login kembali, lalu masuk ke menu "Upload Payment"
9. Silahkan anda Upload bukti pembayaran setiap Polis yang anda Beli

Bagaimana cara Claim Polis ?
1. Silahkan anda Login terlebih dahulu
2. Lalu masuk ke Menu "Claim"
3. Claim hanya dapat dilakukan untuk Polis yang masih aktif
4. Claim dapat dilakukan kembali terhadap Polis yang sama, apabila masih ada sisa Claim

Thank's for your attention
Your Healt is Our Priority