﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.SessionState;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
namespace InsuredStoreDons
{
    public partial class _Default : Page
    {
        public SqlConnection conn;
        public SqlCommand cmd, cmd1, cmdViewProduct, cmdViewNasabah, cmdCekPolis, cmdCekCheckout;
        public SqlDataAdapter da, da1, daViewProduct, daViewNasabah, daCekPolis, daCekCart;
        public DataTable dt;
        public DataSet ds, ds1, dsViewProduct, dsViewNasabah;
        string sql, sql1, sqlcon, sqlCekPolis, sqlCekCheckout, sqlViewProduct, sqlViewNasabah;

        protected void Page_Load(object sender, EventArgs e)
        {
            sqlcon = WebConfigurationManager.ConnectionStrings["koneksiDB"].ToString();
            conn = new SqlConnection(sqlcon);
            viewProduct(); 
        } 
        protected void btnBeli_Click(object sender, EventArgs e)
        {
            string createdDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            conn.Open();

            if(Session["NASABAHID"] != null)
            { 
                sqlCekPolis = "SELECT * FROM POLICY WHERE (POLICYNO ='" + string.Concat(this.productID.Value, Session["NASABAHID"]) + "' AND STATUSPOLICYID ='IN') OR (POLICYNO ='" + string.Concat(this.productID.Value, Session["NASABAHID"]) + "')";
                cmdCekPolis = new SqlCommand(sqlCekPolis, conn);
                SqlDataReader data = cmdCekPolis.ExecuteReader();
                if (data.HasRows)
                {
                    Response.Write("<script>alert('Maaf, Policy Ditolak sudah ada dalam Table Policy') </script>");
                
                }else
                {
                    sqlCekCheckout = "SELECT * FROM CHECKOUT WHERE POLICYNO ='" + string.Concat(this.productID.Value, Session["NASABAHID"]) +"'";
                    cmdCekCheckout = new SqlCommand(sqlCekCheckout, conn);
                    SqlDataReader cek = cmdCekCheckout.ExecuteReader();
                    if (cek.HasRows)
                    {
                        Response.Write("<script>alert('Maaf, Policy sudah ada dalam Checkout.') </script>");

                    }
                    else
                    { 
                        sql = "INSERT INTO CHECKOUT (POLICYNO, PRODUCTID, NASABAHID, STATUSPOLICYID, CREATEDDATE) VALUES('" + string.Concat(this.productID.Value, Session["NASABAHID"]) + "','" + this.productID.Value + "','" + Session["NASABAHID"] + "', 'PE' ,'" + createdDate + "')";

                        cmd = new SqlCommand(sql, conn);
                        int m = cmd.ExecuteNonQuery();

                        if (m != 0)
                        {
                            Response.Write("<script> alert('Policy berhasil pindah Checkout') </script>");
                        }
                        else
                        {
                            Response.Write("<script>alert('Policy gagal pindah Checkout') </script>");
                        }
                     
                    }
                }
            }else
            {
                Response.Write("<script>alert('Silahkan Login Terlibh dahulu') </script>");
            }

            conn.Close(); 
        }
        public void viewProduct()
        {
                conn.Open();
                sqlViewProduct = "SELECT a.*, b.DESCRIPTION FROM PRODUCT a JOIN BENEFIT b ON a.BENEFITID = b.BENEFITID";
                cmdViewProduct = new SqlCommand(sqlViewProduct, conn);
                daViewProduct = new SqlDataAdapter();
                dsViewProduct = new DataSet();
                daViewProduct.SelectCommand = cmdViewProduct;
                daViewProduct.Fill(dsViewProduct);
                RepeaterProduct.DataSource = dsViewProduct;
                RepeaterProduct.DataBind();
                conn.Close(); 
        }  
    }
}