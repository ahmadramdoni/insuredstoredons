﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InsuredStoreDons.Startup))]
namespace InsuredStoreDons
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
