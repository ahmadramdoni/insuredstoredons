﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls; 
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
namespace InsuredStoreDons
{
    public partial class Claim : System.Web.UI.Page
    {
        public SqlConnection conn;
        public SqlCommand cmd, cmdClaim;
        public SqlDataAdapter da, daClaim;
        public DataTable dt;
        public DataSet ds;
        string sql, sqlcon, sqlClaim;
        private object Keys;

        protected void Page_Load(object sender, EventArgs e)
        {
            sqlcon = WebConfigurationManager.ConnectionStrings["koneksiDB"].ToString();
            conn = new SqlConnection(sqlcon);
            viewPolicy();
        }
      
        protected void btnClaim_Click(object sender, EventArgs e)
        {
             
            string createdDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            if (txtJum.Text == "")
            {
                Response.Write("<script> alert('Maaf, Nilai Claim Masih Kosong') </script>");
            }

            else

            {
                if (Int32.Parse(txtJum.Text) >= 2147483647)
                {
                    Response.Write("<script> alert('Maaf, Nilai Claim Melebihi yang Diizinkan') </script>");
                }
                else
                {
                    int nilaiClaimC = Int32.Parse(txtJum.Text);

                    conn.Open();
                    SqlCommand CheckMaxC = new SqlCommand("SELECT TOP(1) a.SISACLAIM AS SISACLAIM, d.SATUAN AS SATUAN, d.POSSATUAN AS POSSATUAN FROM CLAIM a " +
                                                            "JOIN POLICY b ON a.POLICYNO = b.POLICYNO " +
                                                            "JOIN PRODUCT c ON b.PRODUCTID = c.PRODUCTID " +
                                                            "JOIN BENEFIT d ON c.BENEFITID = d.BENEFITID WHERE a.POLICYNO = '" + policyNo.Value + "' ORDER BY a.CLAIMID DESC", conn);
                    SqlDataReader readerC = CheckMaxC.ExecuteReader();
                    if (readerC.HasRows)
                    {
                        while (readerC.Read())
                        {

                            int sisaClaimC = Int32.Parse(readerC["SISACLAIM"].ToString());
                            string satuanClaim = readerC["SATUAN"].ToString();
                            int posSatuan = Int32.Parse(readerC["POSSATUAN"].ToString());

                            int sisaC = sisaClaimC - nilaiClaimC;

                            if (sisaClaimC >= nilaiClaimC)
                            {
                                SqlCommand insertClaimC = new SqlCommand("INSERT INTO CLAIM values('" + policyNo.Value + "', '" + txtJum.Text + "','" + sisaC + "', '" + createdDate + "')", conn);

                                int b = insertClaimC.ExecuteNonQuery();

                                if (b > 0)
                                {
                                    Response.Write("<script> alert('Claim Berhasil') </script>");

                                }
                                else
                                {
                                    Response.Write("<script>alert('Claim Gagal') </script>");
                                }
                            }
                            else
                            {
                                if (posSatuan == 0)
                                {
                                    Response.Write("<script>alert('Maaf, Nilai Claim Melebihi Nilai Sisa Claim Dari Asuransi ini. Sisa Claim Anda : " + satuanClaim + " " + String.Format("{0:n0}", sisaClaimC) + "') </script>");
                                }
                                else if (posSatuan == 1)
                                {
                                    Response.Write("<script>alert('Maaf, Nilai Claim Melebihi Nilai Sisa Claim Dari Asuransi ini. Sisa Claim Anda : " + String.Format("{0:n0}", sisaClaimC) + " " + satuanClaim + "') </script>");
                                }
                                 
                            }
                        }
                    }
                    else
                    {
                        SqlCommand CheckMax = new SqlCommand("SELECT b.MAX AS MAX, b.SATUAN AS SATUAN, b.POSSATUAN AS POSSATUAN FROM PRODUCT a " +
                                                                "JOIN BENEFIT b ON a.BENEFITID = b.BENEFITID " +
                                                                "where a.PRODUCTID = '" + productID.Value + "'", conn);
                        SqlDataReader reader = CheckMax.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                int maxClaim = Int32.Parse(reader["MAX"].ToString());
                                string satuan = reader["SATUAN"].ToString();
                                int posSatuan = Int32.Parse(reader["POSSATUAN"].ToString());

                                int sisa = maxClaim - nilaiClaimC;
                                if (maxClaim >= nilaiClaimC)
                                {
                                    SqlCommand insertClaim = new SqlCommand("INSERT INTO CLAIM values('" + policyNo.Value + "', '" + txtJum.Text + "','" + sisa + "', '" + createdDate + "')", conn);

                                    int b = insertClaim.ExecuteNonQuery();

                                    if (b > 0)
                                    {
                                        Response.Write("<script> alert('Claim Berhasil') </script>");

                                    }
                                    else
                                    {
                                        Response.Write("<script>alert('Claim Gagal') </script>");
                                    }
                                }
                                else
                                {
                                    if (posSatuan == 0)
                                    {
                                        Response.Write("<script>alert('Maaf, Nilai Claim Melebihi Nilai Maximal Claim Dari Asuransi ini. Maximal Claim Anda : " + satuan + " " + String.Format("{0:n0}", maxClaim) + " ') </script>");
                                    }
                                    else if (posSatuan == 1)
                                    {
                                        Response.Write("<script>alert('Maaf, Nilai Claim Melebihi Nilai Maximal Claim Dari Asuransi ini. Maximal Claim Anda : " + String.Format("{0:n0}", maxClaim) + " " + satuan + " ') </script>");
                                    }
                                }
                            }
                        }

                    }
                }

                Server.Transfer("Claim.aspx", false);
            }

        }
        public void viewPolicy()
        { 
                conn.Open();
                sql = "SELECT a.*, b.NAMEPRODUCT, c.DESCRIPTION, d.FULLNAME FROM POLICY a "+
                    "JOIN PRODUCT b ON a.PRODUCTID = b.PRODUCTID "+
                    "JOIN BENEFIT c ON b.BENEFITID = c.BENEFITID "+
                    "JOIN NASABAH d ON a.NASABAHID = d.NASABAHID WHERE a.NASABAHID ='"+ Session["NASABAHID"]+"' AND a.STATUSPOLICYID='IN' ORDER BY a.CREATEDDATE DESC";
                
                cmd = new SqlCommand(sql, conn);
                da = new SqlDataAdapter();
                dt = new DataTable();
                ds = new DataSet();
                da.SelectCommand = cmd; 
                da.Fill(ds);
                listPolicy.DataSource = ds;
                listPolicy.DataBind();

                SqlDataReader cekDt = cmd.ExecuteReader();
                if (cekDt.HasRows)
                {
                     
                    lblEmpty.Visible = false;
                   

                }
                else
                { 
                    lblEmpty.Visible = true;
                }
                 
                conn.Close();
             
        }
    }
}