﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="InsuredStoreDons.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Now, You can Contact Me !</h3>
    <address>
        ISD Tower Floor 2<sup>th</sup><br />
        Doni, WA 087888475732<br />
        <abbr title="Phone">P:</abbr>
        (021) 87443622
    </address>

    <address>
        <strong>Support:</strong>   <a href="mailto:ramdoni.ahmad@gmail.com">ramdoni.ahmad@gmail.com</a><br /> 
    </address>
</asp:Content>
