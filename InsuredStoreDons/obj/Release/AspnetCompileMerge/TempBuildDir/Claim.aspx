﻿<%@ Page Title="Claim Insured"  Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Claim.aspx.cs" Inherits="InsuredStoreDons.Claim" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   
  <div class="row"> 

  </div>
  <div class="row">  
    <div class="col-md-12">
    <asp:ListView ID="listPolicy" runat="server"> 
            <LayoutTemplate> 
                <table class="table table-striped">
                    <tr>
                        <td>POLICYNO</td>
                        <td>NASABAH</td>
                        <td>NAMEPRODUCT</td>
                        <td>DESCRIPTION</td> 
                        <td>-</td>
                    </tr>
                <asp:PlaceHolder ID="itemPlaceHolder" runat="server"/>  
                </table>
                
           </LayoutTemplate>
            <ItemTemplate>
                <tr runat="server">
                    <td><%# Eval("POLICYNO") %></td> 
                    <td><%# Eval("FULLNAME") %></td> 
                    <td><%# Eval("NAMEPRODUCT") %></td> 
                    <td><%# Eval("DESCRIPTION") %></td> 
                    <td><button type="button" id="btnTampilModalClaim" class="btn btn-primary" data-toggle="modal" data-target="#modalClaim" data-policy="<%# Eval("POLICYNO") %>" data-productid="<%# Eval("PRODUCTID") %>">Claim</button>
                     
                    </td>  
                </tr>
                
            </ItemTemplate>
        </asp:ListView>

        <asp:Label runat="server" ID="lblEmpty" Class="lblEmpty" Text="Data Kosong, Anda belum membeli atau melakukan pembayaran Polis..."></asp:Label>
       
       </div>
       </div>
    <div class="modal fade" id="modalClaim" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Silahkan anda isi data dibawah ini.</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>  
            <div class="modal-body">
              <asp:HiddenField ID="productID" runat="server"></asp:HiddenField>   
              <asp:HiddenField ID="policyNo" runat="server"></asp:HiddenField>   
            
              <div class="form-group">  
                  <label for="txtJum" class="form-control-label">Nilai Claim :</label>
                  <asp:TextBox ID="txtJum" type="number" AutoCompleteType="Disabled" runat="server" MaxLength="2147483647"></asp:TextBox>
              </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <asp:Button ID="btnClaim" OnClick="btnClaim_Click" runat="server" class="btn btn-primary" Text="OK" /> 
      </div> 
    </div>
  </div>
</div>
  <script type="text/javascript">
     
      $('#modalClaim').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget)
          var productID = button.data('productid')
          var policyNo = button.data('policy') 
          var modal = $(this) 
          $('#<%= productID.ClientID %>').val(productID);
          $('#<%= policyNo.ClientID %>').val(policyNo); 
      });
 
    </script>
</asp:Content>

