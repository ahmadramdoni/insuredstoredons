﻿<%@ Page Title="Buy Insured" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="InsuredStoreDons._Default" %>
 
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
  
    <div class="jumbotron">
        <h1>InsuredStoreDons</h1>
        <p class="lead">Agent Of Your Secure</p> 
    </div> 
    <div class="row"> 
        <asp:Repeater ID="RepeaterProduct" runat="server">
            <HeaderTemplate>
                <div class="col-md-12"> 
            </HeaderTemplate>
            <ItemTemplate>
                <div class ="col-md-6">
                    <div class="card product">
                        <div class="card-body">  
                                <h3 class="card-title"><%# Eval("NAMEPRODUCT") %></h3>
                                <p class="card-text">
                                    PREMI : Rp. <%# String.Format("{0:n}",  Eval("PREMI")) %><br>
                                    COVERAGE : <%# Eval("COVERAGE") %> Bulan<br>
                                    BENEFIT : <%# Eval("DESCRIPTION") %>
                                </p>
                                <p>
                                   <button type="button" id="btnTampilModalBeli" class="btn btn-primary" data-toggle="modal" data-target="#modalBeli" data-nameproduct="<%# Eval("NAMEPRODUCT") %>" data-productid="<%# Eval("PRODUCTID") %>">Beli Dong!</button>
                                </p>
                        </div>
                    </div>
                </div> 
            </ItemTemplate>
            <FooterTemplate> 
                </div>
            </FooterTemplate> 
        </asp:Repeater>  
       </div>
  <div class="modal fade" id="modalBeli" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Silahkan anda isi data dibawah ini.</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>  
            <div class="modal-body">
              <asp:HiddenField ID="productID" runat="server"></asp:HiddenField>  
              <asp:HiddenField ID="nasabahID" runat="server"></asp:HiddenField>
               <div class="form-group"> 
                  <h3 class="nameProduct"></h3> 
               </div>
              <div class="form-group">  
                  <label for="NasabahID" class="form-control-label">Nasabah ID :</label>
                  <asp:Repeater ID="RepeaterNasabah" runat="server">
                    <HeaderTemplate>
                        <select id="NasabahID" name="NasabahID" onchange ="get_nasabah()">
                    </HeaderTemplate>
                    <ItemTemplate>
                          <!--<input type="text" class="form-control" id="NasabahID">--> 
                        <option value="<%# Eval("NASABAHID") %>"><%# Eval("FULLNAME") %></option> 
                    </ItemTemplate>
                    <FooterTemplate>
                        </select>
                    </FooterTemplate>
                  </asp:Repeater>  
              </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <asp:Button ID="btnBeli" OnClick="btnBeli_Click" runat="server" class="btn btn-primary" Text="Beli" /> 
      </div> 
    </div>
  </div>
</div>
  <script type="text/javascript">
     
      $('#modalBeli').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget)
          var productID = button.data('productid')
          var nameProduct = button.data('nameproduct')
          var modal = $(this) 
          $('#<%= productID.ClientID %>').val(productID);
          modal.find('.nameProduct').html(nameProduct)
      });

      function get_nasabah() { 
              var nasabahID = $('#NasabahID').val();
              $('#<%= nasabahID.ClientID %>').val(nasabahID);
          
      }
      $(document).ready(function () {

          get_nasabah()
      })
    </script>
</asp:Content>
