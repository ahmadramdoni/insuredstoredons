﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls; 
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Threading;
using System.Web.SessionState;
namespace InsuredStoreDons
{
    public partial class ShoppingCart : System.Web.UI.Page
    {
        public SqlConnection conn;
        public SqlCommand cmd, cmdInsertCheckout, cmdJum;
        public SqlDataAdapter da, daJum;
        public DataTable dt;
        public DataSet ds;
        string sql, sqlJum, sqlcon;

        protected void Page_Load(object sender, EventArgs e)
        {
            sqlcon = WebConfigurationManager.ConnectionStrings["koneksiDB"].ToString();
            conn = new SqlConnection(sqlcon);
            viewShoppingCart();
            lblSuccess.Visible = false;
        }
  
        public void jumlahShoppingChart()
        {
            try
            {
                conn.Open();
                sqlJum = "SELECT count (POLICYNO) AS total FROM CHECKOUT";

                cmdJum = new SqlCommand(sqlJum, conn);
                daJum = new SqlDataAdapter(); 
                 
                conn.Close();
            }
            catch (SqlException ex)
            {

            }
        }
        public void viewShoppingCart()
        {
            try
            {
                conn.Open();
                sql = "SELECT a.POLICYNO, b.NAMEPRODUCT, b.PREMI, b.COVERAGE, c.DESCRIPTION FROM CHECKOUT a "+
                    "JOIN PRODUCT b ON a.PRODUCTID = b.PRODUCTID "+
                    "JOIN BENEFIT c ON b.BENEFITID = c.BENEFITID WHERE a.NASABAHID = '"+ Session["NASABAHID"] +"'";
                
                cmd = new SqlCommand(sql, conn);
                da = new SqlDataAdapter();
                ds = new DataSet();
                da.SelectCommand = cmd;
                da.Fill(ds);
                listShoppingCart.DataSource = ds;
                listShoppingCart.DataBind();

                SqlDataReader cekDt = cmd.ExecuteReader();
                if (cekDt.HasRows)
                {
                    
                    btnCheckout.Visible = true;
                    lblEmpty.Visible = false; 

                }
                else
                {
                    btnCheckout.Visible = false;
                    lblEmpty.Visible = true;
                }
                
               
                conn.Close();
            }
            catch (SqlException ex)
            {

            }

        }
        protected void btnCheckout_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                SqlCommand Check = new SqlCommand("SELECT * FROM CHECKOUT", conn);
                SqlDataReader reader = Check.ExecuteReader();
                 
                while (reader.Read())
                {
                    string POLICYNO = reader["POLICYNO"].ToString();
                    string PRODUCTID = reader["PRODUCTID"].ToString();
                    string NASABAHID = reader["NASABAHID"].ToString();
                    string STATUSPOLICYID = reader["STATUSPOLICYID"].ToString();
                    string CREATEDDATE = reader["CREATEDDATE"].ToString();

                    SqlCommand insertCheckout = new SqlCommand("INSERT INTO POLICY values(@POLICYNO, @PRODUCTID, @NASABAHID, @STATUSPOLICYID, @CREATEDDATE)", conn);
                    insertCheckout.Parameters.AddWithValue("@POLICYNO", POLICYNO); 
                    insertCheckout.Parameters.AddWithValue("@PRODUCTID", PRODUCTID);
                    insertCheckout.Parameters.AddWithValue("@NASABAHID", NASABAHID);
                    insertCheckout.Parameters.AddWithValue("@STATUSPOLICYID", STATUSPOLICYID);
                    insertCheckout.Parameters.AddWithValue("@CREATEDDATE", CREATEDDATE); 
                    int b = insertCheckout.ExecuteNonQuery();

                    if (b > 0)
                    {
                        SqlCommand deletedCheckout = new SqlCommand("DELETE FROM CHECKOUT", conn);
                        deletedCheckout.ExecuteNonQuery(); 
                    }
                    else
                    {
                        Response.Write("<script>alert('Checkout Gagal') </script>");
                        
                    }
                    lblSuccess.Visible = true;
                }

                Server.Transfer("ShoppingCart.aspx", false);
                conn.Close();
            }
            catch (SqlException ex)
            {

            }

        }
        protected void timeOff_Tick(object sender, System.EventArgs e)
        {
            lblSuccess.Visible = true;
        }

    }
}