﻿<%@ Page Title="Shopping Cart" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ShoppingCart.aspx.cs" Inherits="InsuredStoreDons.ShoppingCart" %>
 
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   
    <div class="row"> 
        &nbsp;
    </div>
    <div class="row">  
                <div class="col-md-12">
    <asp:ListView ID="listShoppingCart" runat="server">
            <LayoutTemplate> 
                <table class="table table-striped">
                    <tr>
                        <td>POLICYNO</td>
                        <td>NAME PRODUCT</td>
                        <td>PREMI</td>  
                        <td>COVERAGE</td> 
                    </tr>
                <asp:PlaceHolder ID="itemPlaceHolder" runat="server"/>  
                </table>
                
           </LayoutTemplate>
            <ItemTemplate>
                <tr runat="server">
                    <td><%# Eval("POLICYNO") %></td>
                    <td><%# Eval("NAMEPRODUCT") %></td> 
                    <td>Rp. <%# String.Format("{0:n}",  Eval("PREMI"))%></td> 
                    <td><%# Eval("COVERAGE") %> Bulan</td> 
                </tr>
                
            </ItemTemplate>
        </asp:ListView>
                    
        <asp:Label runat="server" ID="lblEmpty" Class="lblEmpty" Text="Data Is Empty"></asp:Label>
        <asp:Label runat="server" ID="lblSuccess" Class="btn btn-success" Text="Terima Kasih..."></asp:Label> 
       <asp:Button ID="btnCheckout" class="btn btn-primary" runat="server" Text="Checkout" OnClick="btnCheckout_Click" />
       </div>
       </div>
</asp:Content>
