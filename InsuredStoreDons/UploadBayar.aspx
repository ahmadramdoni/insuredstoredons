﻿<%@ Page Title="UploadBayar Insured"  Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UploadBayar.aspx.cs" Inherits="InsuredStoreDons.UploadBayar" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   
  <div class="row"> 

  </div>
  <div class="row">  
    <div class="col-md-12">
    <asp:ListView ID="listPolicy" runat="server"> 
            <LayoutTemplate> 
                <table class="table table-striped">
                    <tr>
                        <td>POLICYNO</td>
                        <td>NASABAH</td>
                        <td>NAMEPRODUCT</td>
                        <td>DESCRIPTION</td> 
                        <td>-</td>
                    </tr>
                <asp:PlaceHolder ID="itemPlaceHolder" runat="server"/>  
                </table>
                
           </LayoutTemplate>
            <ItemTemplate>
                <tr runat="server">
                    <td><%# Eval("POLICYNO") %></td> 
                    <td><%# Eval("FULLNAME") %></td> 
                    <td><%# Eval("NAMEPRODUCT") %></td> 
                    <td><%# Eval("DESCRIPTION") %></td>  
                    <td><button type="button" id="btnTampilModalUploadBayar" class="btn btn-primary" data-toggle="modal" data-target="#modalUploadBayar" data-policy="<%# Eval("POLICYNO") %>">Upload Bayar</button>
                     
                    </td>  
                </tr>
                
            </ItemTemplate>
        </asp:ListView>

        <asp:Label runat="server" ID="lblEmpty" Class="lblEmpty" Text="Data Is Empty"></asp:Label>
       
       </div>
       </div>
    <div class="modal fade" id="modalUploadBayar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Silahkan anda isi data dibawah ini.</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>  
            <div class="modal-body">  
              <asp:HiddenField ID="policyNo" runat="server"></asp:HiddenField>   
            
              <div class="form-group">  
                  <label for="UploadBuktiBayar" class="form-control-label">Upload Bukti Bayar Anda:</label>
                  <asp:FileUpload ID="UploadBuktiBayar" runat="server" />
              </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <asp:Button ID="btnUploadBayar" OnClick="btnUploadBayar_Click" runat="server" class="btn btn-primary" Text="OK" /> 
      </div> 
    </div>
  </div>
</div>
  <script type="text/javascript">
     
      $('#modalUploadBayar').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget) 
          var policyNo = button.data('policy') 
          var modal = $(this)  
          $('#<%= policyNo.ClientID %>').val(policyNo); 
      });
 
    </script>
</asp:Content>

