﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using InsuredStoreDons.Models; 
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace InsuredStoreDons.Account
{
    public partial class Register : Page
    {
        public SqlConnection conn; 
        string sqlcon;
        #region myCreateUser
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlcon = WebConfigurationManager.ConnectionStrings["koneksiDB"].ToString();
            conn = new SqlConnection(sqlcon);
            
        }

        private void clear()
        {
            userName.Text = "";
            fullName.Text = "";
            Password.Text = "";
            ConfirmPassword.Text = "";
        }
        protected void CreateUser_Click(object sender, EventArgs e)
        {
            try
            {
                
                conn.Open();
                SqlCommand Check = new SqlCommand("SELECT TOP (1) NASABAHID FROM NASABAH ORDER BY NASABAHID DESC", conn);
                SqlDataReader reader = Check.ExecuteReader();

                while (reader.Read())
                {
                    string NASABAHID = reader["NASABAHID"].ToString();
                    int NoAkhirNew = Int32.Parse(NASABAHID.Substring(3)) + 1;

                    SqlCommand inNasabah = new SqlCommand("INSERT INTO NASABAH values(@NASABAHID, '" + fullName.Text+ "',  '" + userName.Text + "', '" + Password.Text + "')", conn);
                    inNasabah.Parameters.AddWithValue("@NASABAHID", "NSB" + NoAkhirNew);
                     
                    int b = inNasabah.ExecuteNonQuery();

                    if (b > 0)
                    {
                         
                        Response.Write("<script> alert('Nasabah Berhasil di input') </script>");
                        clear();
                        Server.Transfer("Login.aspx", false); 
                    }
                    else
                    {
                        Response.Write("<script>alert('Nasabah Gagal di input') </script>");
                         
                    }


                }
                  
                conn.Close();
            }
            catch (SqlException ex)
            {

            }

        } 
        #endregion
        #region Original CreateUser
        /*
        protected void CreateUser_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();
            var user = new ApplicationUser() { UserName = userName.Text, Email = Email.Text };
            IdentityResult result = manager.Create(user, Password.Text);
            if (result.Succeeded)
            {
                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                //string code = manager.GenerateEmailConfirmationToken(user.Id);
                //string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
                //manager.SendEmail(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>.");

                signInManager.SignIn( user, isPersistent: false, rememberBrowser: false);
                IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
            }
            else 
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }

        */
        #endregion
    }
}