﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.SessionState;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using InsuredStoreDons.Models;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace InsuredStoreDons.Account
{
    public partial class Login : Page
    {
        public SqlConnection conn;
        string sqlcon;
        protected void Page_Load(object sender, EventArgs e)
        {
            sqlcon = WebConfigurationManager.ConnectionStrings["koneksiDB"].ToString();
            conn = new SqlConnection(sqlcon);
            
            RegisterHyperLink.NavigateUrl = "Register";
            // Enable this once you have account confirmation enabled for password reset functionality
            //ForgotPasswordHyperLink.NavigateUrl = "Forgot";
            OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];
            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            if (!String.IsNullOrEmpty(returnUrl))
            {
                RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            }
        }
        private void clear()
        {
            userName.Text = ""; 
            Password.Text = ""; 
        }
        protected void LogIn(object sender, EventArgs e)
        { 
                conn.Open();
                SqlCommand CheckLogin = new SqlCommand("SELECT NASABAHID, FULLNAME FROM NASABAH WHERE USERNAME = '" + userName.Text + "' AND PASSWORD = '" + Password.Text + "'", conn);
                SqlDataReader reader = CheckLogin.ExecuteReader();
                if (reader.HasRows)
                {
                     
                    while (reader.Read())
                    {
                        string NASABAHID = reader["NASABAHID"].ToString();
                        string FULLNAME = reader["FULLNAME"].ToString();
                        setSession(NASABAHID, FULLNAME);
                        Response.Write("<script>alert('Login Berhasil, Terima Kasih " + FULLNAME  + "') </script>");
                        Server.Transfer("~/Default.aspx", false);
                    }
                }
                else
                {
                    Response.Write("<script>alert('Gagal, Username atau Password anda salah.') </script>");
                }

                conn.Close();
           
        }
        public void setSession(string nasabahId, string fullName)
        {
            Session["NASABAHID"] = nasabahId;
            Session["FULLNAME"] = fullName;
        }
        #region Original Login
        /*protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid)
            {
                // Validate the user password
                var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                // This doen't count login failures towards account lockout
                // To enable password failures to trigger lockout, change to shouldLockout: true
                var result = signinManager.PasswordSignIn(Email.Text, Password.Text, RememberMe.Checked, shouldLockout: false);

                switch (result)
                {
                    case SignInStatus.Success:
                        IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                        break;
                    case SignInStatus.LockedOut:
                        Response.Redirect("/Account/Lockout");
                        break;
                    case SignInStatus.RequiresVerification:
                        Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}", 
                                                        Request.QueryString["ReturnUrl"],
                                                        RememberMe.Checked),
                                          true);
                        break;
                    case SignInStatus.Failure:
                    default:
                        FailureText.Text = "Invalid login attempt";
                        ErrorMessage.Visible = true;
                        break;
                }
            }
        } */

        #endregion
    }
}