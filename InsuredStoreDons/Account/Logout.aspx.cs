﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InsuredStoreDons.Account
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.RemoveAll(); 
            Session.Clear();
            Response.Write("<script> alert('Anda Berhasil Logout') </script>");
            //Server.Transfer("~/Default.aspx", false);
         //   Page.Response.Redirect("~/Default.aspx", false);
        }
    }
}