﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
namespace InsuredStoreDons
{
    public partial class UploadBayar : System.Web.UI.Page
    {
        public SqlConnection conn;
        public SqlCommand cmd, cmdUploadBayar;
        public SqlDataAdapter da, daUploadBayar;
        public DataTable dt;
        public DataSet ds;
        string sql, sqlcon, sqlUploadBayar;
        private object Keys;

        protected void Page_Load(object sender, EventArgs e)
        {
            sqlcon = WebConfigurationManager.ConnectionStrings["koneksiDB"].ToString();
            conn = new SqlConnection(sqlcon);
            viewPolicy();
        }

        protected void btnUploadBayar_Click(object sender, EventArgs e)
        {

            string uploadDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            if (UploadBuktiBayar.FileName == "")
            {
                Response.Write("<script> alert('Maaf, File masih Kosong') </script>");
            }

            else

            {
                string time = DateTime.Now.ToString("yyyyMMddHHmmss");
                string fileNameNew = policyNo.Value.ToString() + "_" + time + "_" + UploadBuktiBayar.FileName.ToString();
                UploadBuktiBayar.SaveAs(Server.MapPath("UploadBuktiBayar") + "/" + fileNameNew);
                conn.Open();
                SqlCommand CheckPolis = new SqlCommand("SELECT POLICYNO FROM POLICY WHERE POLICYNO = '" + policyNo.Value + "'", conn);
                SqlDataReader readerC = CheckPolis.ExecuteReader();
                if (readerC.HasRows)
                {
                    SqlCommand updateStatusPolicy = new SqlCommand("UPDATE POLICY SET STATUSPOLICYID = 'IN' WHERE POLICYNO ='" + policyNo.Value + "'", conn);
                    updateStatusPolicy.ExecuteNonQuery();

                    SqlCommand insertUploadBuktiBayar = new SqlCommand("INSERT INTO UPLOADEDBAYAR values('" + policyNo.Value + "', '" + fileNameNew + "', '" + uploadDate + "')", conn);

                    int b = insertUploadBuktiBayar.ExecuteNonQuery();

                    if (b > 0)
                    {
                        Response.Write("<script> alert('Upload Bukti Pembayaran Anda Berhasil... Terima Kasih') </script>");


                    }
                    else
                    {
                        Response.Write("<script> alert('Gagal Upload File') </script>");

                    }

                }

                Server.Transfer("UploadBayar.aspx", false);
            }

        }
        public void viewPolicy()
        {
            conn.Open();
            sql = "SELECT a.*, b.NAMEPRODUCT, c.DESCRIPTION, d.FULLNAME FROM POLICY a " +
                "JOIN PRODUCT b ON a.PRODUCTID = b.PRODUCTID " +
                "JOIN BENEFIT c ON b.BENEFITID = c.BENEFITID " +
                "JOIN NASABAH d ON a.NASABAHID = d.NASABAHID " +
                "WHERE a.NASABAHID ='" + Session["NASABAHID"] + "' AND STATUSPOLICYID='PE' ORDER BY a.CREATEDDATE DESC";

            cmd = new SqlCommand(sql, conn);
            da = new SqlDataAdapter();
            dt = new DataTable();
            ds = new DataSet();
            da.SelectCommand = cmd;
            da.Fill(ds);
            listPolicy.DataSource = ds;
            listPolicy.DataBind();

            SqlDataReader cekDt = cmd.ExecuteReader();
            if (cekDt.HasRows)
            {

                lblEmpty.Visible = false;
            }
            else
            {
                lblEmpty.Visible = true;
            }


            conn.Close();


        }
    }
}